require 'spec_helper'
require 'rake'

describe 'fetch_date' do
  before { Rails.application.load_tasks }

  it { expect { Rake::Task['fetch_data'].invoke }.not_to raise_exception }
end