require 'spec_helper'

RSpec.describe ParsingController, :type => :controller do

  describe 'GET #index' do
    it 'responds successfully with an HTTP 200 status code' do
      get :index, format: :xml
      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
  end

end