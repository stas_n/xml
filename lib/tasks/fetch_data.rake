desc 'Fetch files data'
task :fetch_data => :environment do
  require 'csv'

  Dir.chdir(Rails.root.to_s + '/public/txt_files')
  files = Dir.glob("*.txt")

  files.each do |file|
    csv = CSV::parse(File.open(file) { |f| f.read })
    fields = csv.shift.drop(1)

    cols = IncomingData.columns.map { |c| c.name }
    params = Hash[cols[1..-3].zip(fields)]

    IncomingData.create(params)
  end

  p 'Done'
end