class Output
  require 'nokogiri'

  def self.xml
    data = IncomingData.all
    builder = Nokogiri::XML::Builder.new do |xml|
      xml.root {
        data.each do |o|
          xml.account_data {
            xml.account_id    o.account_id
            xml.date          o.date
            xml.account_type  o.account_type
            xml.activity      o.activity
            xml.position      o.position
            xml.security      o.security
          }

        end
      }
    end
    builder.to_xml(indent: 2)
  end
end


