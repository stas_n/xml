class ParsingController < ApplicationController

  def index
    respond_to do |format|
      format.xml { render xml: Output.xml }
    end
  end
end