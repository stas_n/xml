README

**Ruby** 2.1.0 

**Rails** 4.2.4

**bundle** - intall gems

**rake db:create db:migrate** - Setup db

**rake db:test:prepare** - Setup test db

**rspec** - Run tests

**rake fetch_data** - Populate db from files

**rails s** - Start server

http://0.0.0.0:3000/ - Enjoy

![scr.png](https://bitbucket.org/repo/Bzn8pE/images/3840925234-scr.png)

P.s 
All necessary code inside *Lib* folder