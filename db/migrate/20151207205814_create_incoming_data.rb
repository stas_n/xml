class CreateIncomingData < ActiveRecord::Migration
  def change
    create_table :incoming_data do |t|
      t.string :account_id
      t.string :account_type
      t.string :date
      t.string :activity
      t.string :position
      t.string :security

      t.timestamps null: false
    end
  end
end
